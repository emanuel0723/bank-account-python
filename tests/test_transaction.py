from transaction import Transaction
import constant as c


def test_transaction():
    """
    Creates one Transaction of type DEPOSIT and one Transaction of type WITHDRAWAL and asserts their properties' values
    """
    t_deposit = Transaction(c.DEPOSIT, 100, 100)
    assert t_deposit.type == c.DEPOSIT
    assert t_deposit.amount == 100
    assert t_deposit.current_balance == 100

    t_withdrawal = Transaction(c.WITHDRAWAL, -50, 50)
    assert t_withdrawal.type == c.WITHDRAWAL
    assert t_withdrawal.amount == -50
    assert t_withdrawal.current_balance == 50
