from datetime import datetime


class Transaction:
    """
    Transaction definition class
    """
    def __init__(self, _type, _amount, _balance):
        """

        :param _type: int (TRANSACTION_TYPES)
        :param _amount: float
        :param _balance: float
            Current global balance after applying the transaction
        """
        self.type = _type
        self.amount = _amount
        self.datetime = datetime.now()
        self.current_balance = _balance
