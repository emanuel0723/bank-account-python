# Bank Account Management System
This project is a Python program to run a Bank Account Management System (**BAMS**). Note: The goal of this project is
 to work as an exercise and is not aimed for a production environment. It has many improvement areas and all comments
 are welcome.
 
## Versions
This project was created and tested with the following versions:
* Python 3.7.4
* Pytest 6.0.1

## Scope
The Bank Account Management System simulates a very simple personal bank account, allowing the user's interaction in a 
text based environment or in a basic GUI.
The operations implemented are:
* Deposit and Withdraw money
   * The user can deposit or withdraw money.
* Print statement
   * The user can print the account's status. The status is shown as a list of the latest transactions ordered by
   newest to oldest.
* Statement filters
   * The user can filter the transactions to show by transaction type and date range.
   
## Out of scope
The Bank Account Management System has some nice to have features that are out of scope for this stage:
* Multi user not supported
   * No login system supported for different personal account management
* Multi account management per user
   * A user has only one bank account
* Account status storage
   * Every execution of the program runs a new bank account management (no transactions registered)
* Logging
   * No logging system included

## Usage
The BAMS can be run as a text based application or using a Graphical User Interface. Both options implement the same
backend functions.

### Text based
Execute the file `app.py` with Python. The program shows a python console with the instructions to execute account transactions and
see the account status.

Command format: `[Action] [Transaction type] [Start date] [End date]`
* **Action**:
    * 1 = Deposit
    * 2 = Withdraw
    * 3 = Print account status
    * 4 = Print help
    * 'exit' = Close the application

* **Transaction type** (optional for **Action** = 3 only): 
    * 1 = All (default)
    * 2 = Deposits
    * 3 = Withdrawals

* **Start date** (optional for **Action** = 3 only): dd-MM-YYYY

* **End date** (optional for **Action** = 3 only): dd-MM-YYYY
    
Important: to filter by date range, all parameters are required. If any parameter is invalid, it is ignored

**Examples** to print account status:  
* Print all transactions `3`
* Print filtered transactions from 27-May-2020 to 18-August-2020: `3 1 27-05-2020 18-08-2020`
* Print filtered deposits from 23-January-2020 to 15-December-2020: `3 2 23-01-2020 15-12-2020`
* Print filtered withdrawals from 1-January-2020 to 31-December-2020: `3 3 1-01-2020 31-12-2020`

### GUI
Execute the file `gui.py` with Python. The program shows a window similar to the following example:

![image](gui_example.jpg)

To create a transaction, use the Amount entry field to set the amount and click Deposit or Withdraw buttons.

To show the balance, press the Show balance button. 

## Program structure
The project files can be understood as two main blocks, frontend and backend. The idea is to keep the code as simple as
possible, with a bit more detailed documentation within each file, with the goal of making them self explanatory.

### Fronted
The Frontend part is the program part in charge of the user interaction. The files responsible for it are `app.py` or
 `gui.py`, depending on the execution style chosen.
 
### Backend
The Backend is formed by the following levels: User -> Account -> Transaction. The **User** class is defined in the 
`user.py` file and is responsible for executing the account functions, create transactions and query the list of 
transactions already created. The **Account** class is defined in `account.py` file and implements functions to create
transactions and to filter the list of transactions. Finally, the **Transaction** class is defined in the 
`transaction.py` file and defines the Transaction object, which stores the parameters of a deposit or withdrawal.

### Common files
The program makes use of the `constant.py` file to store some defined values and reuse them within the different
classes.

## Tests
The test set is executed using a Python console from the project's folder and include tests for the Backend part of the
program. Note, this requires pytest installed, tested with version pytest 6.0.1.
```
> pytest -v
```
