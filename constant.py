TRANSACTION_TYPES = [DEPOSIT, WITHDRAWAL, ALL] = [1, 2, 3]
'''
Transaction types, 0 - Deposit, 1 - WITHDRAWAL
'''

USER_ACTIONS = [DEPOSIT_ACTION, WITHDRAW_ACTION, STATUS, HELP, EXIT] = ['1', '2', '3', '4', 'EXIT']
'''
Valid actions accepted as user input
'''

ACTION_RESULTS = [SUCCESS, INSUFFICIENT_BALANCE, OTHER] = [1, -1, -2]
'''
Action results for user action requests
'''

AMOUNT_COLUMN_CHARS = 15
'''
Number of chars in the amount column (used in the function to print account status)
'''

BALANCE_COLUMN_CHARS = 15
'''
Number of chars in the balance column (used in the function to print account status)
'''