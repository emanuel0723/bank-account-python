from account import Account
from transaction import Transaction
import constant as c
import random
from datetime import datetime
from datetime import timedelta


def random_transactions(n=1000, account=Account(), start_date=None, delta_mins=60):
    """Auxiliary function for the tests to create a specified number of Transactions (random Deposits or Withdrawals)

    Uses the argument 'account' for the creation of transactions.
    The transaction type is a random selection between Deposit and Withdrawal
    The transactions amount is a random float number between 1.0 and 100000.0
    The transaction date is by default, the current datetime of creation. If 'start_date' is provided, the first
    transaction created will have 'start_date' as its datetime and the consecutive transactions will have an increase
    of the last datetime + 'delta_mins'.

    :param n: int
        Number of transaction to create
    :param account: Account
        Account object to create the transactions
    :param start_date: datetime
        Datetime for the first transaction
    :param delta_mins:
        Difference in minutes between transactions datetimes
    :return: float
        Balance after transactions
    """

    # Current balance before random transactions
    test_balance = account.balance
    for _ in range(n):
        transaction_type = random.randint(0, 1)
        test_amount = random.uniform(1.0, 100000.0)
        if transaction_type == c.DEPOSIT:
            if account.deposit(test_amount) == c.SUCCESS:
                test_balance += test_amount
                if start_date is not None:
                    start_date += timedelta(seconds=60 * delta_mins)
                    account.transactions[-1].datetime = start_date
                    account.deposits[-1].datetime = start_date
        else:
            if account.withdraw(test_amount) == c.SUCCESS:
                test_balance -= test_amount
                if start_date is not None:
                    start_date += timedelta(seconds=60 * delta_mins)
                    account.transactions[-1].datetime = start_date
                    account.withdrawals[-1].datetime = start_date

    return test_balance


class TestAccount:
    """
    Class to test Account object methods
    """

    def test_account(self):
        """Test the Account object creation and its parameters values
        :return: None
        """
        account = Account()
        assert account.balance == 0
        assert len(account.transactions) == 0

    def test_deposit(self):
        """Test deposit function in the account
        :return: None
        """
        account = Account()
        success = account.deposit(100)
        # Assert value
        assert success == c.SUCCESS
        assert account.balance == 100
        # Assert types of last transaction and last deposit
        assert type(account.transactions[-1]) is Transaction
        assert type(account.deposits[-1]) is Transaction

    def test_withdraw(self):
        """Test withdraw function in the account.
        First creates a withdrawal with insufficient account balance to test failure and afterwards creates
        successful deposit and withdrawal
        :return: None
        """
        account = Account()
        # Test withdrawal with balance 0 (To fail)
        success = account.withdraw(30)
        # Assert value
        assert success == c.INSUFFICIENT_BALANCE
        assert account.balance == 0
        assert len(account.transactions) == 0

        # Test withdrawal with balance 100 (To succeed)
        success = account.deposit(100)
        success = account.withdraw(30)
        # Assert value
        assert success == c.SUCCESS
        assert account.balance == 70
        # Assert types of last transaction and last withdrawal
        assert type(account.transactions[-1]) is Transaction
        assert type(account.withdrawals[-1]) is Transaction

    def test_create_random_transactions(self):
        """Test the creation of a number of random transactions.
        Validates the account's balance and lists of transactions
        :return: None
        """
        account = Account()

        # Generate random transactions in the account
        test_balance = random_transactions(1000, account)

        # Validate account balance and length of lists
        assert test_balance == account.balance
        assert len(account.transactions) == len(account.deposits) + len(account.withdrawals)

    def test_get_all_deposits(self):
        """Test function to get all account's deposits
        :return: None
        """
        account = Account()
        _ = random_transactions(1000, account)
        deposits = account.get_all_deposits()
        for deposit in deposits:
            assert type(deposit) is Transaction
            assert deposit.amount >= 0

    def test_get_all_withdrawal(self):
        """Test function to get all account's withdrawals
        :return: None
        """
        account = Account()
        _ = random_transactions(1000, account)
        withdrawals = account.get_all_withdrawals()
        for withdrawal in withdrawals:
            assert type(withdrawal) is Transaction
            assert withdrawal.amount < 0

    def test_date_binary_search(self):
        """Test auxiliary function to binary search the index of a datetime in the account's transactions list
        First creates a list of transaction with increasing dates and then asserts the expected index of searching
        a set of datetime cases.
        :return: None
        """
        account = Account()
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 5, 9, 0, 0)
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 7, 9, 0, 0)
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 10, 9, 0, 0)
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 14, 9, 0, 0)
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 15, 9, 0, 0)
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 20, 9, 0, 0)
        account.deposit(10)
        account.transactions[-1].datetime = datetime(2020, 10, 22, 9, 0, 0)
        assert account.find_index(account.transactions, datetime(2020, 10, 5, 9, 0, 0)) == 0
        assert account.find_index(account.transactions, datetime(2020, 10, 7, 9, 0, 0)) == 1
        assert account.find_index(account.transactions, datetime(2020, 10, 10, 9, 0, 0)) == 2
        assert account.find_index(account.transactions, datetime(2020, 10, 14, 9, 0, 0)) == 3
        assert account.find_index(account.transactions, datetime(2020, 10, 15, 9, 0, 0)) == 4
        assert account.find_index(account.transactions, datetime(2020, 10, 20, 9, 0, 0)) == 5
        assert account.find_index(account.transactions, datetime(2020, 10, 22, 9, 0, 0)) == 6

        assert account.find_index(account.transactions, datetime(2020, 10, 5, 7, 0, 0)) == 0
        assert account.find_index(account.transactions, datetime(2020, 10, 30, 9, 0, 0)) == len(account.transactions)
        assert account.find_index(account.transactions, datetime(2020, 10, 8, 7, 0, 0)) == 2
        assert account.find_index(account.transactions, datetime(2020, 10, 20, 14, 0, 0)) == 6
        assert account.find_index(account.transactions, datetime(2020, 10, 22, 9, 0, 1)) == 7

    def test_transactions_by_period(self):
        """Test to filter transactions by a range of dates
        Steps to test:
            1 - Create Account object and random transactions with increasing datetimes of 1 day
            2 - Define the period of time (start and end dates) to filter
            3 - Filter ALL transactions by period of time and assert the resulted datetimes
            4 - Filter Deposits by period of time and assert the resulted datetimes
            5 - Filter Withdrawals by period of time and assert the resulted datetimes

        :return: None
        """
        account = Account()
        # Create transactions with specific dates (Random in October 2020)
        range_start_date = datetime(2020, 10, 1)
        random_transactions(1000, account, range_start_date, 1440) # Delta mins 1440 to register 1 per day

        # Define dates period to filter
        period_start_date = datetime(2020, 10, 5, 9, 0, 0)
        period_end_date = datetime(2020, 10, 10, 23, 59, 59).replace(hour=23, minute=59, second=59)

        # TESTING ALL TRANSACTIONS filtered by period
        transactions = account.filter_transactions(transaction_type=c.ALL, start=period_start_date, end=period_end_date)

        # Assert the result
        for transaction in transactions:
            assert transaction.datetime >= period_start_date
            assert transaction.datetime <= period_end_date
        # -------------------------------------------------------

        # TESTING DEPOSITS ONLY filtered by period
        transactions = account.filter_transactions(transaction_type=c.DEPOSIT, start=period_start_date,
                                                   end=period_end_date)

        # Assert the result
        for transaction in transactions:
            assert transaction.type == c.DEPOSIT
            assert transaction.amount >= 0
            assert transaction.datetime >= period_start_date
            assert transaction.datetime <= period_end_date
        # -------------------------------------------------------

        # TESTING WITHDRAWALS ONLY filtered by period
        transactions = account.filter_transactions(transaction_type=c.WITHDRAWAL, start=period_start_date,
                                                   end=period_end_date)

        # Assert the result
        for transaction in transactions:
            assert transaction.type == c.WITHDRAWAL
            assert transaction.amount < 0
            assert transaction.datetime >= period_start_date
            assert transaction.datetime <= period_end_date
        # -------------------------------------------------------

#tester = TestAccount()
#tester.test_date_binary_search()
#tester.test_transactions_by_period()

