from account import Account
import constant as c


class User:
    """
    Class defining an User object

    Attributes
    ----------
    id : int
        Attribute to store user id (For scalability purposes)
    name : str
        Attribute to store the user name (For scalability purposes)
    bank_account : Account
        Account object associated to the user

    Methods
    -------
    get_balance()
        Returns the user's account balance
    create_transaction(transaction_type, amount)
        Function to create the provided transaction type in the account
    get_transactions()
        Return the list of all Transaction objects in the account
    get_filtered_transactions(transaction_type=c.ALL, start=None, end=None)
        Return the list of Transaction objects filtered by transaction_type, start date and end date

    """

    def __init__(self):
        """
        Initializes the User object, creating its Account object
        Attributes 'id' and 'name' are currently not used, but works as example of parameter definition for future
        scalability.
        """
        self.id = None
        self.name = None
        self.bank_account = Account()

    def get_balance(self):
        """Returns the user's account balance
        :return: float
        """
        return self.bank_account.balance

    def create_transaction(self, transaction_type, amount):
        """Function to create the provided transaction type in the account

        :param transaction_type: int (TRANSACTION_TYPES)
        :param amount: float
        :return: int (ACTION_RESULTS)
        """
        if transaction_type == c.DEPOSIT:
            return self.bank_account.deposit(amount)
        elif transaction_type == c.WITHDRAWAL:
            return self.bank_account.withdraw(amount)

    def get_transactions(self):
        """Return the list of all Transaction objects in the account

        :return: list (list of Transaction objects)
        """
        return self.bank_account.get_all_transactions()

    def get_filtered_transactions(self, transaction_type=c.ALL, start=None, end=None):
        """Return the list of Transaction objects filtered by transaction_type, start date and end date

        Filters in O(log2(n)) where n is the number of Transaction objects in the selected list of transactions

        :param transaction_type: int (TRANSACTION_TYPES)
        :param start: datetime
        :param end: datetime
        :return: list (list of Transaction objects)
        """
        return self.bank_account.filter_transactions(transaction_type, start, end)

