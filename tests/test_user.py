from user import User
import constant as c


class TestUser:
    """
    Class to test User object methods
    """

    def test_name(self):
        """Test User object creation
        :return: None
        """
        user = User()
        assert user.name is None

    def test_get_balance(self):
        """Test get balance method
        :return: None
        """
        user = User()
        assert user.get_balance() == 0

    def test_add_deposit(self):
        """Test deposit creation
        :return: None
        """
        user = User()
        success = user.create_transaction(c.DEPOSIT, 500)
        assert success == c.SUCCESS
        assert user.get_balance() == 500

    def test_add_withdrawal(self):
        """Test withdrawal creation
        First attempting to create a withdrawal with insufficient balance in the account and after, creating first a
        deposit to successfully withdraw later
        :return: None
        """
        user = User()
        # Test failure withdrawal (No money available)
        success = user.create_transaction(c.WITHDRAWAL, 200)
        assert success == c.INSUFFICIENT_BALANCE
        assert user.get_balance() == 0

        # Test valid withdrawal after a deposit
        success = user.create_transaction(c.DEPOSIT, 500)
        success = user.create_transaction(c.WITHDRAWAL, 200)
        assert success == c.SUCCESS
        assert user.get_balance() == 300
