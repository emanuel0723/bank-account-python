from datetime import datetime
from user import User
import constant as c


class App:
    """
    App class to provide text based interactive methods to use the Bank Account Management Service

    Attributes
    ----------
    user : User
        User object to interact with the bank account
    active : Boolean
        Flag to indicate if the app should run or stop

    Methods
    -------
    get_balance()
        Returns the user's account balance
    create_transaction(transaction_type, amount)
        Function to create the provided transaction type in the account
    get_transactions()
        Return the list of all Transaction objects in the account
    get_filtered_transactions(transaction_type=c.ALL, start=None, end=None)
        Return the list of Transaction objects filtered by transaction_type, start date and end date

    """

    def __init__(self):
        """
        App constructor to create new User instance and set the app active
        """
        self.user = User()
        self.active = True

    def run(self):
        """Main application loop

        Requests the user for an action to be validated and processed.
        When the flag 'active' is set to False, the loop finishes

        :return: None
        """
        print('Welcome to your Bank Account Management system\n')
        while self.active:
            action = action_request().upper()
            if validate_action(action):
                self.process_action(action)

    def process_action(self, action):
        """Parses and processes the action requested

        :param action: str
        :return: None
        """

        action_params = action.split(' ')
        # Exit
        if action_params[0] == c.EXIT:
            self.active = False

        # Print help information
        elif action_params[0] == c.HELP:
            print_help()

        # Print account status, filtered if requested
        elif action_params[0] == c.STATUS:
            transaction_type = parse_transaction_type(action_params)
            start_date = parse_date(action_params, 2)
            end_date = parse_date(action_params, 3)
            # If end date provided, default hour set to the end of the day 23:59:59
            if end_date:
                end_date = end_date.replace(hour=23, minute=59, second=59)
            transactions = self.user.get_filtered_transactions(transaction_type, start_date, end_date)
            print('|        Date          |      Amount     |     Balance     |')
            for transaction in reversed(transactions):
                amount = create_amount_string(c.AMOUNT_COLUMN_CHARS, transaction.amount)
                # Balance printed is the global balance when the transaction applied (Behavior to be discussed)
                balance = create_amount_string(c.BALANCE_COLUMN_CHARS, transaction.current_balance)
                print('| ' + transaction.datetime.strftime("%d/%m/%Y, %H:%M:%S") + ' | ' + amount + ' | ' + balance + ' |')

            print('')

        # Deposit or Withdraw
        else:
            # Request for the amount to deposit, if invalid input, asks again
            amount = amount_request()
            while amount == -1:
                amount = amount_request()

            # Cancels the action or creates the transaction
            if amount == 'c' or amount == 'C':
                # Cancel
                return
            else:
                res = self.user.create_transaction(int(action), amount)
                if res == c.SUCCESS:
                    print('Transaction successfully registered')
                elif res == c.INSUFFICIENT_BALANCE:
                    print('Transaction failed. Insufficient balance for the requested withdrawal amount.')
                elif res == c.OTHER:
                    print('Transaction failed. Please try again.')


def action_request():
    """Requests an input action from the user to be validated and processed
    :return: str user input
    """
    print('\nPlease choose an action:')
    action = input('  [1 = Deposit, 2 = Withdraw, 3 = Print account status, 4 = Help, \'exit\' = exit application]: ')
    return action


def amount_request():
    """Requests an input amount for the transaction to be created or cancel the action

    :return: float amount
    """
    amount = input('  Please introduce a valid amount or \'C\' to cancel: ')
    if amount == 'c' or amount == 'C':
        return 'C'
    try:
        result = float(amount)
    except:
        return -1
    return result


def print_help():
    """Prints help information about command's format
    :return: None
    """
    print('----- Help -----')
    print('\tCommand format: [Action] [Transaction type] [Start date] [End date]')
    print('\t               Action: [1 = Deposit, 2 = Withdraw, 3 = Print account status, 4 = Print help, exit = exit]')
    print('\t   Transaction type *: [1 = All (default), 2 = Deposits, 3 = Withdrawals]')
    print('\t         Start date *: \'dd-MM-YYYY\'')
    print('\t           End date *: \'dd-MM-YYYY\'')
    print('')
    print('\t   * (optional for action 3 only)')
    print('')
    print('\tImportant: to filter by date range, all parameters are required')
    print('\t           If any parameter is invalid, it is ignored')
    print('')
    print('\tExamples to print account status:')
    print('')
    print('\t   Print all transactions \'3\'')
    print('\t   Print filtered transactions from 27-May-2020 to 18-August-2020: \'3 1 27-05-2020 18-08-2020\'')
    print('\t   Print filtered deposits from 23-January-2020 to 15-December-2020: \'3 2 23-01-2020 15-12-2020\'')
    print('\t   Print filtered withdrawals from 1-January-2020 to 31-December-2020: \'3 3 1-01-2020 31-12-2020\'')
    print('')


def validate_action(action):
    '''
    Validates action requested by user.
    :param action: str
    :return: Boolean False - Invalid action, True - Valid action
    '''
    action_params = action.split(' ')
    if len(action_params) == 0 or action_params[0] not in c.USER_ACTIONS:
        print('Invalid action, please try again')
        # logging.info('User input invalid: %s', str(action))
        return False

    return True


def create_amount_string(chars, amount):
    """Function to construct the amount string with a specific length (to print it aligned)

    :param chars: int result str number of characters
    :param amount: float
    :return: str constructed string with the amount
    """
    amount_str = str(amount)
    left_spaces = ' ' * (chars - len(amount_str))
    return left_spaces + amount_str


def parse_transaction_type(action_params):
    """Parses the transaction type from the list of action params

    Transaction type must be the index 1 of action_params lists and be any of {'1', '2', '3'}

    :param action_params: list[str]
    :return: int (TRANSACTION_TYPES)
    """
    if len(action_params) > 1 and action_params[1] in {'1', '2', '3'}:
        if action_params[1] == '1':
            return c.ALL
        elif action_params[1] == '2':
            return c.DEPOSIT
        else:
            return c.WITHDRAWAL
    else:
        return None


def parse_date(action_params, idx):
    """Parses a date parameter from the list of action params

    The date to parse is at index 'idx' of action_params lists and must be in the format dd-mm-YYYY

    :param action_params: list[str]
    :param idx: int mapped as: 2 - Start date, 3 - End date
    :return:
    """
    if len(action_params) > idx:
        try:
            _date = datetime.strptime(action_params[idx], '%d-%m-%Y')
            return _date
        except:
            return None
    else:
        return None


app = App()
app.run()
