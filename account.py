from transaction import Transaction
import constant as c


class Account:
    """
    Class defining an Account object

    Attributes
    ----------
    transactions : List[Transaction]
        List with the reference to all the transactions in the account
    deposits : List[Transaction]
        List with the reference to all the deposits in the account
    withdrawals : List[Transaction]
        List with the reference to all the withdrawals in the account
    balance : float
        Global account balance

    Methods
    -------
    deposit(_amount)
        Creates a DEPOSIT Transaction and appends its reference to 'transactions' and 'deposits' lists
    withdraw(_amount)
        Creates a WITHDRAWAL Transaction and appends its reference to 'transactions' and 'withdrawals' lists
    get_all_transactions()
        Returns all transactions in the account
    get_all_deposits()
        Returns the list of all deposits in the account
    get_all_withdrawals()
        Returns the list of all withdrawals in the account
    find_index(records, _date)
        Binary search the index corresponding to the '_date' specified in the 'records' list
    filter_transactions(transaction_type=c.ALL, start=None, end=None)
        Filters the transactions in the account by the specified parameters
    """
    def __init__(self):
        """
        Account contructor, initializes balance and transactions lists
        """
        self.transactions = []
        self.deposits = []
        self.withdrawals = []
        self.balance = 0

    def deposit(self, _amount):
        """Creates a DEPOSIT Transaction and appends its reference to 'transactions' and 'deposits' lists

        :param _amount: float
        :return: int (ACTION_RESULTS)
        """
        transaction = Transaction(c.DEPOSIT, _amount, self.balance + _amount)
        self.transactions.append(transaction)
        self.deposits.append(transaction)
        self.balance += _amount
        return c.SUCCESS

    def withdraw(self, _amount):
        """Creates a WITHDRAWAL Transaction and appends its reference to 'transactions' and 'withdrawals' lists

        If the account balance is >= than the _amount parameter, creates the Transaction and returns SUCCESS,
        else, return INSUFFICIENT_BALANCE

        :param _amount: float
        :return: int (ACTION_RESULTS)
        """
        if self.balance - _amount >= 0:
            transaction = Transaction(c.WITHDRAWAL, -_amount, self.balance - _amount)
            self.transactions.append(transaction)
            self.withdrawals.append(transaction)
            self.balance -= _amount
            return c.SUCCESS
        else:
            return c.INSUFFICIENT_BALANCE

    def get_all_transactions(self):
        """Returns the list of all transactions in the account
        :return: list
        """
        return self.transactions

    def get_all_deposits(self):
        """Returns the list of all deposits in the account
        :return: list
        """
        return self.deposits

    def get_all_withdrawals(self):
        """Returns the list of all withdrawals in the account
        :return: list
        """
        return self.withdrawals

    def find_index(self, records, _date):
        """Function to binary search the index corresponding to the _date specified in the records list

        Runs in O(n) time where n is the number of transactions in the records list. For this to work, the records list
        must be in ascending order by datetime.

        :param records: list (list of Transaction objects)
        :param _date: datetime
        :return:
        """
        l = 0
        r = len(records) - 1
        while l <= r:
            m = l + (r - l)//2
            # If the record is not a Transaction, the records list is invalid, return -1
            if type(records[m]) != Transaction:
                return -1

            if records[m].datetime == _date:
                return m
            elif records[m].datetime > _date:
                r = m - 1
            else:
                l = m + 1

        return l

    def filter_transactions(self, transaction_type=c.ALL, start=None, end=None):
        """Filters the transactions in the account by the specified parameters

        The provided transaction type selects the list to filter, then, finds the corresponding indexes for start
        and end dates within the elements in the list, to finally return the range of transactions defined by the index

        :param transaction_type: int TRANSACTION_TYPES
        :param start: datetime
        :param end: datetime
        :return: list (list of Transaction objects)
        """
        # Selects the list to filter according to transaction_type
        if not transaction_type or transaction_type == c.ALL:
            to_filter = self.transactions
        elif transaction_type == c.DEPOSIT:
            to_filter = self.deposits
        elif transaction_type == c.WITHDRAWAL:
            to_filter = self.withdrawals
        else:
            to_filter = []

        # Find the limit index of the corresponding Transaction object in the list to_filter
        start_idx = 0 if not start else self.find_index(to_filter, start)
        end_idx = len(to_filter) if not end else self.find_index(to_filter, end)

        # Returns the filtered list
        return to_filter[start_idx:end_idx]
