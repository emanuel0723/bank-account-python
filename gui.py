#!/usr/bin/python

import tkinter as tk
from tkinter import ttk
from datetime import datetime
from scrollable import TextScrollCombo
from user import User
import constant as c


class GUI(tk.Frame):
    """
    Main class to provide a GUI to use the Bank Account Management Service.

    Attributes
    ----------
    master : tkinter root object
    user : User
        User object to interact with the bank account
    account_status_field : TextScrollCombo
        TextScrollCombo object to print the account status in a scrollable text field

    Methods
    -------
    create_transaction(transaction_type)
        Creates the provided transaction type in the user's account
    create_account_status_list(records)
        Fills the 'account_status_field' with the records list provided
    show_balance()
        Parses the filters fields, gets the filtered list of transactions and calls create_account_status_list
        to show the account status
    create_widgets()
        Function executed in the class constructor to create all the GUI's fields

    """

    def __init__(self, master=None):
        """GUI constructor to create new User instance and all the window's components

        :param master: tkinter root
        """
        master.resizable(0, 0)
        super().__init__(master)
        self.master = master
        self.pack()
        self.user = User()
        self.account_status_field = TextScrollCombo()
        self.create_widgets()

    def create_transaction(self, transaction_type):
        """Function to create a transaction type in the account

        This function parses the value in the 'entry_amount' field and then it creates the transaction using
        the User's method 'create_transaction'.
        Finally, the function creates a message to show the transaction's result to be shown in the window

        :param transaction_type: int (TRANSACTION_TYPES) value attached to the button
        :return: int (ACTION_RESULTS)
        """
        amount = self.entry_amount.get()
        message_transaction = ['', 'Deposit', 'Withdrawal'][transaction_type] + ' for ' + amount
        if validate_number_input(amount):
            transaction_result = self.user.create_transaction(transaction_type, float(amount))
            if transaction_result == c.SUCCESS:
                self.entry_amount.delete(0, 'end')
                message_result = 'Successful transaction'
                self.label_trans_message.config(fg="green")
            elif transaction_result == c.INSUFFICIENT_BALANCE:
                message_result = 'Insufficient balance'
                self.label_trans_message.config(fg="orange")
            else:
                message_result = 'Transaction failed, try again'
                self.label_trans_message.config(fg="red")
        else:
            message_result = 'Amount introduced error'
            self.label_trans_message.config(fg="red")

        self.label_trans_message['text'] = message_transaction + '\n' + message_result
        self.label_trans_message.place(x=420, y=40)

    def create_account_status_list(self, records):
        """Fills the 'account_status_field' with the records list provided

        This function handles the account_status_field, cleans first the previous value and fills it again with the
        filtered list of transactions, formatting the string to fit the defined table.

        :param records: list[Transaction]
        :return: None
        """

        # Cleans and Makes the account status field editable
        self.account_status_field.pack(fill="both", expand=True)
        self.account_status_field.txt.config(state='normal')
        self.account_status_field.txt.delete('1.0', tk.END)
        self.account_status_field.config(width=570, height=200)
        self.account_status_field.txt.config(font=("consolas", 12), undo=True, wrap='word')

        style = ttk.Style()
        style.theme_use('clam')

        # Creates the account status list of transactions
        self.account_status_field.txt.insert(tk.END, '|        Date          |      Amount     |     Balance     |\n')
        for transaction in reversed(records):
            amount = create_amount_string(c.AMOUNT_COLUMN_CHARS, transaction.amount)
            # Balance printed is the global balance when the transaction applied (Behavior to be discussed)
            balance = create_amount_string(c.BALANCE_COLUMN_CHARS, transaction.current_balance)
            date_str = transaction.datetime.strftime("%d/%m/%Y, %H:%M:%S")
            self.account_status_field.txt.insert(tk.END, '| ' + date_str + ' | ' + amount + ' | ' + balance + ' |\n')

        # Disables the account status field to avoid user editions
        self.account_status_field.txt.config(state=tk.DISABLED)

    def show_balance(self):
        """Parses the filters fields, gets the filtered list of transactions and calls 'create_account_status_list'
        to show the account status

        :return: None
        """
        transaction_type = [c.ALL, c.DEPOSIT, c.WITHDRAWAL][self.combo_transaction_type.current()]
        start_date = parse_date(self.entry_start_date.get())
        end_date = parse_date(self.entry_end_date.get())
        # If end date provided, default hour set to the end of the day 23:59:59
        if end_date:
            end_date = end_date.replace(hour=23, minute=59, second=59)

        transactions = self.user.get_filtered_transactions(transaction_type, start_date, end_date)

        self.create_account_status_list(transactions)

    def create_widgets(self):
        """Function executed in the class constructor to create all the GUI's fields

        In this function, all the components are placed in the window and configured

        :return: None
        """
        # TRANSACTIONS SECTION ------------------------------------------
        canvas1 = tk.Canvas(width=570, height=140)
        canvas1.place(x=0, y=0)
        canvas1.pack()

        label_title1 = tk.Label(text='Transactions', font='Helvetica 14 bold')
        canvas1.create_window(70, 20, window=label_title1)

        self.entry_amount = tk.Entry(justify='right')
        label_amount = tk.Label(text='Amount: ', font='Helvetica 10 bold')
        self.entry_amount.config(width=16)
        canvas1.create_window(100, 70, window=label_amount)
        canvas1.create_window(190, 70, window=self.entry_amount)

        btn_deposit = tk.Button(text='Deposit', command=lambda: self.create_transaction(c.DEPOSIT), font='Helvetica 10 bold')
        btn_deposit.config(width=12)
        canvas1.create_window(330, 50, window=btn_deposit)

        btn_withdraw = tk.Button(text='Withdraw', command=lambda: self.create_transaction(c.WITHDRAWAL), font='Helvetica 10 bold')
        btn_withdraw.config(width=12)
        canvas1.create_window(330, 90, window=btn_withdraw)

        label_last_trans_result = tk.Label(text='Last transaction result:', font='Helvetica 8 bold')
        label_last_trans_result.place(x=420, y=20)
        self.label_trans_message = tk.Label(text='Successful transaction', font='Helvetica 10', justify=tk.LEFT)

        # ---------------------------------------------------------------

        separator = ttk.Separator(self.master, orient='horizontal')
        separator.pack(side='top', fill='x')

        # FILTERS SECTION ------------------------------------------
        canvas2 = tk.Canvas(width=590, height=150)
        canvas2.place(x=0, y=110)
        canvas2.pack()

        label_title2 = tk.Label(text='Account Status', font='Helvetica 14 bold')
        canvas2.create_window(90, 20, window=label_title2)

        label_transaction_type = tk.Label(text='Transactions: ', font='Helvetica 10 bold')
        canvas2.create_window(90, 70, window=label_transaction_type)

        self.combo_transaction_type = ttk.Combobox(self.master, state="readonly", font='Helvetica 10')
        self.combo_transaction_type.place(x=150, y=205)
        self.combo_transaction_type["values"] = ["All", "Deposits", "Withdrawals"]
        self.combo_transaction_type.current(0)
        self.combo_transaction_type.config(width=11)

        label_date_range = tk.Label(text='Date range: ', font='Helvetica 10 bold')
        canvas2.create_window(320, 70, window=label_date_range)

        label_placeholder_date1 = tk.Label(text='DD-MM-YYYY', font='Helvetica 8')
        label_placeholder_date2 = tk.Label(text='DD-MM-YYYY', font='Helvetica 8')
        canvas2.create_window(410, 50, window=label_placeholder_date1)
        canvas2.create_window(520, 50, window=label_placeholder_date2)

        self.entry_start_date = tk.Entry(justify='center')
        self.entry_start_date.config(width=14)
        canvas2.create_window(410, 70, window=self.entry_start_date)
        self.entry_end_date = tk.Entry(justify='center')
        self.entry_end_date.config(width=14)
        canvas2.create_window(520, 70, window=self.entry_end_date)

        btn_balance = tk.Button(text='Show balance', command=self.show_balance, font='Helvetica 10 bold')
        canvas2.create_window(500, 110, window=btn_balance)


def validate_number_input(number):
    """Function to validate if the input string can be converted to a float number

    :param number: str
    :return: Boolean
    """
    try:
        float(number)
        return True
    except ValueError:
        return False


def create_amount_string(chars, amount):
    """Function to construct the amount string with a specific length (to print it aligned)

    :param chars: int result str number of characters
    :param amount: float
    :return: str constructed string with the amount
    """
    amount_str = str(amount)
    left_spaces = ' ' * (chars - len(amount_str))
    return left_spaces + amount_str


def parse_date(date_value):
    """Function to validate if the input string can be converted to a datetime object and return it

    :param date_value: str
    :return: datetime
    """
    try:
        _date = datetime.strptime(date_value, '%d-%m-%Y')
        return _date
    except ValueError:
        return None


top = tk.Tk()
gui = GUI(top)
gui.mainloop()
